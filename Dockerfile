FROM golang:1.16.3-stretch AS builder

COPY . /app/blog/
WORKDIR /app/blog

# CGO enabled so we can use the static ld flags
# This lets us build a static binary, as our final container
# will have nothing to link to

RUN go get github.com/magefile/mage & \
    GOBIN=$GOPATH/bin go get && \
    CGO_ENABLED=0 mage buildr && \
    addgroup --system  nonroot && \
    useradd -g nonroot nonroot

# Copy over our built binary as well as our non root user
FROM scratch
COPY --from=builder /app/blog/ /app/blog/
COPY --from=builder /etc/passwd /etc/passwd

WORKDIR /app/blog/

ARG PORT
CMD ["./bin/blog"]
