package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

var (
	Timestamp string
	Hash      string
)

type server struct {
	posts     blogList
	router    *http.ServeMux
	port      string
	mode      string
	timestamp string
	hash      string
}

func main() {
	run_mode := flag.String("mode", "prod", "Enables hot reloading blog posts, and rough draft folder")
	flag.Parse()

	port := os.Getenv("PORT")
	if port == "" {
		port = "12345"
	}

	var b blogList
	server := server{
		router:    http.NewServeMux(),
		posts:     b,
		port:      port,
		mode:      *run_mode,
		timestamp: Timestamp,
		hash:      Hash,
	}

	POSTS_FOLDERS := []string{"./posts/public/"}

	// Hot reloading is useful when writing a post
	if server.mode == "dev" {
		log.Print("Enabling hot reload of blog posts on edit")
		POSTS_FOLDERS = append(POSTS_FOLDERS, "./posts/rough")
		m := monitor{watch: POSTS_FOLDERS}
		go m.Watch(server.posts.populateIndex)
	}

	err := server.posts.populateIndex(POSTS_FOLDERS)
	if err != nil {
		log.Panic(fmt.Errorf("Could not generate posts: %v!", err))
	}

	// Attach our routes and let it fly
	server.routes()
	srv := &http.Server{
		Addr:         ":" + server.port,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  120 * time.Second,
		Handler:      server.router,
	}
	log.Printf("Server running on %s", server.port)
	log.Printf("Server running in mode: %s", server.mode)
	log.Fatal(srv.ListenAndServe())
}
