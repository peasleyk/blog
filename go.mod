module gitlab.com/peasleyk/blog

go 1.13

require (
	github.com/fsnotify/fsnotify v1.5.4
	github.com/gomarkdown/markdown v0.0.0-20220627144906-e9a81102ebeb
	github.com/magefile/mage v1.13.0
	golang.org/x/sys v0.0.0-20220708085239-5a0f0661e09d // indirect
	golang.org/x/text v0.3.7
)
