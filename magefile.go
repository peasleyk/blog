//go:build mage
// +build mage

// Mage build script for My blog. See options below
package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"time"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

// Globals used in jobs below
var Default = Dev

var BINARY_NAME = "blog"
var BINARY_PATH = filepath.Join(BUILD_DIR, BINARY_NAME)
var BROWSER = "firefox"
var BUILD_DIR = "./bin"
var GITLAB_KEY_VAR = "GKEY"
var JOB_LIMIT = 5
var PROD_URL = "blog.peasley.me"
var PROJECT_ID = "16821722"
var RELEASE_PARAMS = "-s -w"

// Response of gitlab jobs
type Job struct {
	Ref        string `json:"ref"`
	Created_at string `json:"created_at"`
	Name       string `json:"name"`
	Status     string `json:"status"`
}

// Builds the binary.
func Build() error {
	gocmd := mg.GoCmd()
	return sh.RunV(gocmd, "build", "-o", BINARY_PATH, "-ldflags="+flags())
}

func flags() string {
	timestamp := time.Now().Format(time.RFC3339)
	hash, _ := sh.Output("git", "rev-parse", "--short", "HEAD")
	return fmt.Sprintf(`-X "main.Timestamp=%s" -X "main.Hash=$$%s"`, timestamp, hash)
}

// Builds a release version with more checks
func BuildR() error {
	mg.Deps(Clean, All)
	gocmd := mg.GoCmd()
	return sh.RunV(gocmd, "build", "-o", BINARY_PATH, "-ldflags="+RELEASE_PARAMS+" "+flags())
}

// Starts the development server and opens firefox
func Dev() (err error) {
	mg.Deps(Build)
	port := port()
	if err := sh.RunV(BROWSER, fmt.Sprintf("localhost:%s", port)); err != nil {
		return err
	}
	if err := sh.RunV(BINARY_PATH, "-mode", "dev"); err != nil {
		return err
	}
	return nil
}

// Open up the live site
func Live() (err error) {
	return sh.RunV(BROWSER, PROD_URL)
}

// Run the server in production mode
func Run() error {
	Build()
	port := port()
	envs := map[string]string{"PORT": port}
	if err := sh.RunWith(envs, BINARY_PATH, "-mode", "prod"); err != nil {
		return err
	}
	return nil
}

// Remove the temporarily generated files from Build.
func Clean() error {
	cmd := mg.GoCmd()
	err := sh.RunV(cmd, "clean")
	err = sh.Rm(BUILD_DIR)
	if err != nil {
		return err
	}
	return nil
}

// Builds and runs a docker container
func Docker() error {
	port := port()
	envs := map[string]string{"DOCKER_BUILDKIT": "1"}
	if err := sh.RunWith(envs, "podman", "build", "--force-rm", "-t", BINARY_NAME+":latest", "."); err != nil {
		return err
	}
	if err := sh.RunV("podman", "run", "--rm", "--init", "-p", fmt.Sprintf("%s:%s", port, port), BINARY_NAME+":latest"); err != nil {
		return err
	}
	return nil

}

// Runs JS prettier
func pretty() error {
	if err := sh.RunV("prettier", "-w", "./static"); err != nil {
		return err
	}
	return nil
}

// Tools to run before committing
func All() {
	cmd := mg.GoCmd()
	// Neat way to kick off a list of commands
	defer func() {
		{
			sh.RunV(cmd, "fmt")
			sh.RunV(cmd, "vet")
			sh.RunV(cmd, "mod", "tidy")
			sh.RunV(cmd, "mod", "verify")
			sh.RunV("golangci-lint", "run")
			sh.RunV(cmd, "clean")
			pretty()
		}
	}()
}

func Upgrade() error {
	cmd := mg.GoCmd()
	if err := sh.RunV(cmd, "get", "-u"); err != nil {
		return err
	}
	if err := sh.RunV(cmd, "mod", "tidy"); err != nil {
		return err
	}
	return nil
}

// Determines what port to use
func port() string {
	p := os.Getenv("PORT")
	if p == "" {
		return "12345"
	}
	return p
}

// Status of gitlab pipeline
func Status() {
	jobs := getJobs()
	res := maxes(jobs)
	if res == nil {
		println("No jobs found")
		return
	}
	len_total := 0
	for _, v := range res {
		len_total += v
	}

	println("+---" + strings.Repeat("-", len_total+len(res)) + "----+")
	for x := 0; x < JOB_LIMIT; x++ {
		e := reflect.ValueOf(&jobs[x]).Elem()
		out := ""
		for i := 0; i < e.NumField(); i++ {
			name := e.Type().Field(i).Name
			value := e.Field(i).String()
			if i == e.NumField()-1 {
				out += fmt.Sprintf("| %-*s |", res[name], value)
			} else {
				out += fmt.Sprintf("| %-*s ", res[name], value)
			}
		}
		println(out)
	}
	println("+---" + strings.Repeat("-", len_total+len(res)) + "----+")
}

// Dynamically gets all max lengths of struct fields
// A little hacky but I'm a little lazy
func maxes(j []Job) map[string]int {
	m := make(map[string]int)
	if len(j) < 1 {
		return nil
	}

	for x := range j {
		// reflect.Value
		e := reflect.ValueOf(&j[x]).Elem()
		for i := 0; i < e.NumField(); i++ {
			// Type contains a structField
			name := e.Type().Field(i).Name
			// still in reflect.value
			value := e.Field(i).String()
			if m[name] == 0 {
				m[name] = len(value)
				continue
			}
			if len(value) > m[name] {
				m[name] = len(value)
			}
		}
	}

	return m
}

// Calls gitlabs jobs API
func getJobs() []Job {
	client := &http.Client{}
	req, _ := http.NewRequest("GET", fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/jobs", PROJECT_ID), nil)
	req.Header.Set("PRIVATE-TOKEN", os.Getenv(GITLAB_KEY_VAR))
	resp, err := client.Do(req)
	if err != nil {
		return nil
	}
	defer resp.Body.Close()

	var jobs []Job
	json.NewDecoder(resp.Body).Decode(&jobs)

	if len(jobs) < 1 {
		println("No jobs found")
		return nil
	}
	return jobs
}
