package main

import (
	"log"

	"github.com/fsnotify/fsnotify"
)

type monitor struct {
	watch []string
}

// Example fits perfectly with use case
// https://github.com/fsnotify/fsnotify/blob/master/example_test.go
func (m *monitor) Watch(fn func([]string) error) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()

	// watcher.Events and watcher.Errors are both channels. We wait in a blocking loop until
	// we read a message from these channels and handle them
	// TODO - why does this fire twice?
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					log.Println("Something's wrong with our file watcher")
					return
				}
				if event.Op&fsnotify.Write == fsnotify.Write {
					log.Printf("modified file: %s", event.Name)
				}
				err := fn([]string{"./posts/*/"})
				if err != nil {
					println(err.Error())
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					log.Println("Error reading from ... errors?")
					return
				}
				log.Printf("error: %v", err)
			}
		}
	}()

	for _, f := range m.watch {
		err = watcher.Add(f)
		if err != nil {
			log.Fatal(err)
		}

	}

	// We want this go routine to keep running, so lets use a blocking select
	select {}
}
