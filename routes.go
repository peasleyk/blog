package main

import (
	"bytes"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strings"
)

// Page information, aka what we template in
type Page struct {
	Title        string
	Posts        []post
	Path         string
	Body         template.HTML
	ErrorMessage string
	Found        bool
	Timestamp    string
	Hash         string
}

var templates = template.Must(template.ParseFiles(
	"static/templates/index.tmpl",
	"static/templates/projects.tmpl",
	"static/templates/table.tmpl",
	"static/templates/header.tmpl",
	"static/templates/footer.tmpl",
	"static/templates/404.tmpl",
	"static/templates/view.tmpl"))

// Root handler for our paths
func (s *server) root(w http.ResponseWriter, r *http.Request) {
	switch path := r.URL.Path; path {
	case "/":
		s.index(w, r)
	case "/projects/":
		s.projects(w, r)
	default:
		s.handlerError(w, r, fmt.Errorf("404, invalid page"), http.StatusNotFound)
	}

}

// Main page
func (s *server) index(w http.ResponseWriter, r *http.Request) {
	page := Page{
		Title:     "Blog",
		Posts:     s.posts,
		Hash:      s.hash,
		Timestamp: s.timestamp,
	}
	err := renderTemplate(w, "index", page)
	if err != nil {
		s.handlerError(w, r, fmt.Errorf("Error rendering template: %v", err), http.StatusInternalServerError)
	}
}

// Project list
func (s *server) projects(w http.ResponseWriter, r *http.Request) {
	page := Page{
		Title: "Blog",
	}
	err := renderTemplate(w, "projects", page)
	if err != nil {
		s.handlerError(w, r, fmt.Errorf("Error rendering template: %v", err), http.StatusInternalServerError)
	}
}

// View a blog post
func (s *server) view(w http.ResponseWriter, r *http.Request) {
	splitPath := strings.Split(r.URL.Path, "/")
	blogRequest := splitPath[2]

	var page Page
	for _, post := range s.posts {
		if post.FileName == blogRequest {
			page = Page{
				Title: post.Title,
				Body:  template.HTML(post.Body),
				Found: true,
			}
		}
	}

	if !page.Found {
		s.handlerError(w, r, fmt.Errorf("404: %s", blogRequest), http.StatusInternalServerError)
		return
	}

	err := renderTemplate(w, "view", page)
	if err != nil {
		s.handlerError(w, r, fmt.Errorf("Error rendering template: %v", err), http.StatusInternalServerError)
	}
}

// If anything fails (or a non existent route comes up), throw a 404 page up
func (s *server) handlerError(w http.ResponseWriter, r *http.Request, err error, code int) {
	log.Println(err)
	w.WriteHeader(http.StatusNotFound)
	data := Page{
		Title:        "Error",
		ErrorMessage: "Try again later",
	}
	// This shouldn't really fail
	err = renderTemplate(w, "404", data)
	if err != nil {
		panic(err)
	}
}

// Render a template with some data
func renderTemplate(w http.ResponseWriter, tmpl string, d interface{}) error {
	// ExecuteTemplate takes in a io.Writer, which bytes.buffer happens to
	// satisfy io.Writer interface. Therefore to avoid having a half rendered template
	// we can 'try' writing to that buffer. If that works, we can then
	// write that buffer to our ResponseWriter
	// https://stackoverflow.com/q/30821745/
	buf := &bytes.Buffer{}
	err := templates.ExecuteTemplate(buf, tmpl+".tmpl", d)
	if err != nil {
		return err
	}
	// Template is fine, go and write it out
	_, err = buf.WriteTo(w)
	if err != nil {
		return err
	}

	return nil
}

// Sets up our routes to our router
func (s *server) routes() {
	s.router.HandleFunc("/", s.root)
	s.router.HandleFunc("/projects/", s.root)
	s.router.HandleFunc("/post/", s.view)
	s.router.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
}
