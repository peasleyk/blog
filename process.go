package main

import (
	"io/ioutil"
	"log"
	"path/filepath"
	"sort"
	"strings"

	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/html"
	"golang.org/x/text/cases"
	"golang.org/x/text/language"
)

// Holds our posts methods
type blogList []post // A list of posts to show

// Information on a file
type post struct {
	Title    string
	Date     string // For sorting
	Year     string
	FileName string
	Body     string
}

// Finds all the individual recipe folders by the meta file
func getPosts(postFolders []string) ([]string, error) {
	var vp []string
	for _, folder := range postFolders {
		posts, err := filepath.Glob(filepath.Join(folder, "*.md"))
		if err != nil {
			return nil, err
		}
		vp = append(vp, posts...)
	}

	return vp, nil
}

// Grabs post information from a filename
func parseDetails(markdownPath string) (string, string, string) {
	_, file := filepath.Split(markdownPath)
	file = strings.TrimSuffix(file, filepath.Ext(file))
	parts := strings.Split(file, "_")
	title := cases.Title(language.English, cases.NoLower).String(strings.ReplaceAll(parts[0], "-", " "))
	// title := cases.Title()
	date := parts[1]
	return title, date, file
}

// For each post folder, populate meta information
func (b *blogList) populateIndex(postFolders []string) error {
	// When we enable hot reloading, we need to empty the current list of posts
	// to rebuild them
	*b = (*b)[:0]

	log.Println("Populating blog posts...")
	posts, err := getPosts(postFolders)
	if err != nil {
		return err
	}

	// Lets go over each file to pull elements from the filename
	// and generate the HTML to display
	for _, postPath := range posts {
		title, date, fileName := parseDetails(postPath)
		year := "2019"
		file, err := ioutil.ReadFile(postPath)
		opts := html.RendererOptions{
			RenderNodeHook: highlightHook,
		}
		renderer := html.NewRenderer(opts)
		postHTML := string(markdown.ToHTML(file, nil, renderer))
		if err != nil {
			return err
		}

		post := post{
			Date:     date,
			Year:     year,
			Title:    title,
			FileName: fileName,
			Body:     postHTML,
		}
		*b = append(*b, post)
	}

	// Now, we want our blog post to display in order
	sort.Slice(*b, func(i, j int) bool {
		return (*b)[i].Date > (*b)[j].Date
	})

	return nil
}
