# Yet Another Task Runner

Another day, another person writes a makefile replacement as he's been using basically just .PHONY and it was overkill. I've used Mage with go and enjoyed it a lot, so I [built one for python](https://gitlab.com/peasleyk/taskr). Install it,  ```taskr_cli```

## Taskr

It's a CLI to run tasks in a specific file (tasks.py in the project root). Tasks are any and every function in this file, unless prefixed with `_`. There are also a few handy functions to make system calls (Even in venvs seamlessly!)

Here's a very small example of a task.py file
```python
import taskr

# Tasker will not list this
def _get_system():
  return os.environ.get("build_system")

def build():
  env = _get_system()
  taskr.run_env("python setup.py install", env={"BUILD_SYSTEM": env})
```

And how to run it
```shell
[master●] » taskr build
```

Pretty simple. It even generates a dummy tasks.py file for you (cool!)

```shell
[master●] » taskr --init
```

And lists tasks out (wow!) with the task name being the name of the function

```shell
[master●] » taskr -l

Tasks:
 all      : Runs all static analysis tools
 build    : Builds the wheel
 clean    : Remove build artifacts, cache, etc.
 flake    : Check flake8
 format   : Run black
 mypy     : Checks types
 reinstall: Reinstalls taskr
 sort     : Sort imports
 *test    : Run tests

* = default

```

## Why?

It's a stripped down version of [mage](https://magefile.org/), it was fun to make, and I think it would be easy to use for people on my team rather than make or a bash script, and it's easy to spin up a new file for a new project with nice built-ins (Task listing, generating)