# VScode Is Still Lying to You

It's 2022 and VScode is still technically not open source. It does have a [MIT license](https://github.com/microsoft/vscode/blob/main/LICENSE.txt), yes, but don't be fooled. That's only if you build it yourself through cloning. If you download straight from their website the license changes - https://code.visualstudio.com/License/

Suddenly you're agreeing to a bespoke Microsoft license with much more agreements. Personal data collection statements, states Microsoft's reserves all rights and you only get a few. You cannot

 - Reverse engineer the editor
 - Remove notices of Microsoft
 - Share/Rent/Lease/Publish the software
 
 All of these wildly different that the MIT license you thought you had.


### Caveats With Building Yourself

You can still build the editor yourself if you don't want to agree to this license with a few stipulations
- You cannot use the official Visual Studio Marketplace
- You cannot use official Visual studio plugins that aren't bundled with the editor, if they run through Mircosoft servers

You can still install opensource plugins manually but that's a pain in the ass.

### What To Do?

Good news is someone maintains a truly open source version of VScode, [VSCodium](https://vscodium.com/). It's a pre-built version of VScode ready to use with a third party marketplace for extensions. There's also [THEI](https://theia-ide.org/), a forked version of VScode

### Why Care At All?

It's a bit disingenuous to market VSCode as open source when in reality it's not. They may not market it as truly open source (they say built on open source) and almost everyone associates it as such. It's been something that's irked me and turned me off of using it and I wanted to rant a bit. 

I really with there was a truly large opensource UI based editor for everyone to rally around (Like atom, gVIm, eclipse), but it seems like VScode is eating that markets lunch. I think my current favorite up-and-comer is https://lapce.dev/

I'll keep being a hypocrite and using closed source Sublime 4, because at lease they are upfront.