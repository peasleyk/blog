# Glitch: What's Git?

Recently I put together a joke website for a community, and didn't want my real life identity to be apart of it (I try to keep my real life and online gaming presence identity decoupled). I have a VPS but didn't want to throw it on there, so I thought GitHub pages would be easiest since they already had an organization. I made a new GitHub account, new SSH keys, and put the repo on the site. Before I could set up pages my new account was banned, with no reason. Huh.

I've heard about [glitch](https://glitch.com/) before, and decided to try it out. It's basically a community focused website hosting platform, that encourages remixing and sharing projects. I clicked new project, pasted my code, and I magically had a live website with a link. No account, no servers, no git. 

![pic](../static/images/glitch.png)

Coming from someone who loves git, setting up VM's, hacking away on network things, this was super cool. Low friction, easy for users to understand (they have a live preview window), and for power-users a full web terminal where your site is running. You can also build more powerful apps with a database, a real server, etc. More importantly, the site and design is just _fun_.

All this to say, I started programming when there is some tech that has definitely improved a ton. I think I should spend more time modernizing my way of working, rather than being tied up in the lower level sys admin things :). Git and all is cool, but having websites like this for newer developers (and non developers!) to quickly build and share things is always future.
