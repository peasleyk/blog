# FZF And You

You use [fzf](https://github.com/junegunn/fzf), right? Do you want to be a cool dev? If you answered no and yes go read that link and come back. Okay, now that you're converted here's a dump of my most used functions, some copied and some original. 

```bash

# Generic 

# List processes, press enter to kill. Much faster than HTOP
function fkill () {
  local pid
  pid=$(ps -ef | sed 1d | fzf -m | awk '{print $2}') 
  if [ "x$pid" != "x" ]
  then
    echo $pid | xargs kill -${1:-9}
  fi
}


# Cat dir and show preview
function fcat {
  fzf --preview '[[ $(file --mime {}) =~ binary ]] &&
                 echo {} is a binary file ||
                 (highlight -O ansi -l {} ||
                  coderay {} ||
                  rougify {} ||
                  cat {}) 2> /dev/null | head -5'
}

# Command history, enter selects
function fh {
  eval $( ([ -n "$ZSH_NAME" ] && fc -l 1 || history) | fzf +s --tac | sed 's/ *[0-9]* *//')
}

# Edit file in $EDITOR
function fedit() {
  file=$(fzf)
  # Open the file if it exists
  if [ -n "$file" ]; then
    # Use the default editor if it's defined, otherwise Vim
    ${EDITOR:-vim} "$file"
  fi
}

# Grep the current dir and open the line in an editor
function fgrep(){
  FILE=$(ag --nobreak --noheading . | fzf -0 -1 | awk -F: '{print $1}')
  $EDITOR $FILE
}

#  Git specific

# Change branch
function fbranch() {
  git for-each-ref --format='(refname:short)' refs/heads | fzf | xargs git checkout
}

# Show commit history, and preview the diff. Enter to checkout.
function fcheck() {
  local commit
  commit=$( glNoGraph |
    fzf --no-sort --reverse --tiebreak=index --no-multi \
        --ansi --preview $_viewGitLogLine ) &&
  git checkout $(echo "$commit" | sed "s/ .*//")
}

# Show commit history, and preview the diff. Enter for full view.
alias glNoGraph='git log --color=always --format="%C(auto)%h%d %s %C(black)%C(bold)%cr% C(auto)%an" "$@"'
local _gitLogLineToHash="echo {} | grep -o '[a-f0-9]\{7\}' | head -1"
local _viewGitLogLine="$_gitLogLineToHash | xargs -I % sh -c 'git show --color=always % '"

function flog() {
    glNoGraph |
        fzf --no-sort --reverse --tiebreak=index --no-multi \
            --ansi --preview $_viewGitLogLine \
                --header "enter to view, alt-y to copy hash" \
                --bind "enter:execute:$_viewGitLogLine   | less -R" \
                --bind "alt-y:execute:$_gitLogLineToHash | xclip"
}


```

Go forth and copy, create, and invent your own. I recommend making a seperate `fzf.sh` file and sourcing it in your RC file.