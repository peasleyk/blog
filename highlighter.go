package main

import (
	"fmt"
	"io"
	"strings"
	"text/scanner"

	"github.com/gomarkdown/markdown/ast"
)

//Generic list of keywords I want highlighted
var keywords = []string{
	"def", "function", "func", "go", "package", "import", "var", "if", "while", "type",
}

// List if symbols to be highlighted
var symbols = []string{
	"!", "<", ">", "?", ":", "&", "=", "|", "&&", "^", "-", "+=", "+", "$",
}

// Shouldn't ever have a non nil error..
func handle(err error) {
	if err != nil {
		panic(err)
	}
}

// Attaches to gomarkdowns generator and overrides the code block renderer.
// to add highlighting
func highlightHook(w io.Writer, node ast.Node, entering bool) (ast.WalkStatus, bool) {
	if _, ok := node.(*ast.CodeBlock); !ok {
		return ast.GoToNext, false
	}
	// custom rendering logic for ast.CodeBlock
	cb := node.(*ast.CodeBlock)
	if string(cb.Info) != "" {
		_, err := io.WriteString(w, "<pre class='code'>")
		handle(err)
		out := scan(string(cb.Info), string(node.AsLeaf().Literal))
		_, err = io.WriteString(w, out)
		handle(err)
		_, err = io.WriteString(w, "</pre>")
		handle(err)
	}

	return ast.GoToNext, true
}

// Is a string in a list?
func in(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// Utility function that wraps text with a span
func color(token string, syntax_type string) string {
	return fmt.Sprintf("<span class='%s'>%s</span>", syntax_type, token)
}

// Similar to scans Comment, only for pound symbols instead
// Can be a bit smarter and make sure line starts with a pound also
// if issues arise
// This basically processes a line of text as a custom type
func pound_comment(s *scanner.Scanner) string {
	comment := "<span class='comment'>"
	var tok rune
	for tok != '\n' {
		comment += s.TokenText()
		tok = s.Scan()
	}
	// Since we took out the newline.. add it back in
	// This lets us not spill over and scan the next line by mistake
	comment += "\n</span>"
	return comment
}

// TODO
//  - some sort of struct/enum for code type and keywords
//  - python doc strings support
// Large Frankenstein switch statement for highlighter rules
func scan(code_type string, src string) string {
	var s scanner.Scanner
	s.Init(strings.NewReader(src))
	// Do nothing with errors for now
	s.Error = func(s *scanner.Scanner, msg string) {
	}
	// Don't skip comments, we want to highlight those
	s.Mode ^= scanner.SkipComments

	// We want to process spaces, new lines, and tabs in the output
	// Without this, they will be stripped out
	s.Whitespace ^= 1<<'\t' | 1<<'\n' | 1<<' '

	var tok rune
	var out string
	for tok != scanner.EOF {
		tok = s.Scan()
		tok_string := s.TokenText()
		switch {
		case tok == '#':
			if code_type == "python" || code_type == "bash" {
				out += pound_comment(&s)
			} else {
				out += tok_string
			}
		case in(tok_string, keywords):
			out += color(tok_string, "keyword")
		case tok == scanner.Int:
			out += color(tok_string, "int")
		case tok == scanner.String || tok == scanner.Char:
			out += color(tok_string, "string")
		case tok == scanner.Comment && code_type != "bash":
			out += color(tok_string, "comment")
		case in(s.TokenText(), symbols):
			out += color(tok_string, "symbol")
		default:
			out += tok_string
		}
	}
	return out
}
